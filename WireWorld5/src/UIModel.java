	import java.awt.Component;


	public class UIModel {
	   
	   private Circuit board;
	   private FileManager fileManager;
	   private Stan stateManager;

	    public UIModel() {
	        fileManager = new FileManager();
	        board = new Circuit(fileManager);
	        stateManager = new Stan(board);
	    }

	    public int getStateIn(int row, int column) {
	        return this.board.getCellIn(row, column).getStan();
	    }

	    public Cell getCellIn(int row, int column) {
	        return this.board.getCellIn(row, column);
	    }


	    public int getRows() {
	        return this.board.getRows();
	    }


	    public int getColumns() {
	        return this.board.getColumns();
	    }


	    public Boolean boardIsPopulated() {
	        return this.board.isPopulated();
	    }


	    public void loadFile(String filePath) throws Exception {
	        board.populateFromFile(filePath);
	    }

	    public void updateOnce() {
	            stateManager.updateOnce();
	    }

	    public void toggleContinuosUpdate() {
	            stateManager.toggleContinuosUpdate();
	    }

	    public void stopContinuosUpdate() {
	        stateManager.stopContinuosUpdate();
	    }

	    public void setSpeed(int speed) {
	        stateManager.setSpeed(speed); 
	    }
	}
