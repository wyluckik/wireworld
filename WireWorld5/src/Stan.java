	import java.util.*;
	import java.awt.*;

	public class Stan {

	    private long updatesPerSecond;
	    private Boolean isContinuoslyUpdating;
	    private Circuit board;
	    private Timer stateTimer;
	    private TimerTask task;

	    public Stan(Circuit board) {
	        this.board = board;
	        this.updatesPerSecond = 15;
	        this.isContinuoslyUpdating = false;
	        this.stateTimer = new Timer(true);
	    }

	    public void setSpeed(int speed) {
	        updatesPerSecond = (long)(speed);
	        if (isContinuoslyUpdating) {
	            this.task.cancel();
	            isContinuoslyUpdating = false;
	            toggleContinuosUpdate();
	        }
	    }
	    
	    public void updateOnce() {
	        if (!isContinuoslyUpdating) {
	            stateTimer.schedule(new Update(), new Date());
	        }
	    }

	     public void stopContinuosUpdate() {
	        if (isContinuoslyUpdating) {
	            this.task.cancel();
	            isContinuoslyUpdating = false;
	        }
	     }

	    public void toggleContinuosUpdate() {
	        if (isContinuoslyUpdating) {
	            stopContinuosUpdate();
	        } else {
	            long period = 1000 / updatesPerSecond;
	            this.task = new Update();
	            stateTimer.schedule(this.task, new Date(), period);
	            isContinuoslyUpdating = true;
	        }
	    }

	    class Update extends TimerTask {
	        public void run() {
	            for (int row = 0; row < board.getRows(); row++) {
	                for (int col = 0; col < board.getColumns(); col++) {
	                    board.getCellIn(row, col).evolve();
	                }
	            }
	            for (int row = 0; row < board.getRows(); row++) {
	                for (int col = 0; col < board.getColumns(); col++) {
	                    board.getCellIn(row, col).checkHood();
	                }
	            }
	        }
	    }
	}