	import java.util.Observable;


	public class Cell extends Observable{
	    private Boolean change;
	    private Cell[] sasiedztwo;
	    private int state;

	    public Cell(int stan) {
	        super();
	        setStan(stan);
	        setHood(null);
	        change = true;
	    }

	    public boolean equals(Cell komorka) {
	        return (komorka.getStan() == this.state);
	    }

	    public int getStan() {
	        return this.state;
	    }

	    public void setStan(int nowystan) {
	        if (nowystan == 1 || nowystan == 2 || nowystan == 3) {
	            this.state = nowystan;
	        } else {
	            this.state = 0;
	        }
	    }

	    public void setHood(Cell[] neighbourhood) {
	        this.sasiedztwo = neighbourhood;
	    }

	    public Cell evolve() {
	        if (state == 1) {
	        	state = 2;
	            this.stateChanged();
	        } else if (state == 2) {
	        	state = 3;
	            this.stateChanged();
	        } else if (state == 3) {
	            if (change) {
	            	state = 1;
	                this.stateChanged();
	            } else {
	            	state = 3;
	            }
	        }
	        return this;
	    }

	    public void stateChanged() {
	        this.setChanged();
	        this.notifyObservers();
	    }

	    public boolean checkHood() {
	        int heads = 0;
	        if ( this.sasiedztwo == null) {
	           this.change = false; 
	           return false;
	        }
	        for (Cell i : this.sasiedztwo) {
	            if (i != null && i.getStan() == 1) {
	                    heads++;
	            }
	        }
	        this.change = (heads == 1 || heads == 2);
	        return (heads == 1 || heads == 2);
	    }
	}

