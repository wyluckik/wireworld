	import java.awt.event.*;
	import javax.swing.event.*;
	import javax.swing.*;
	import java.io.File;

	public class GUIController {
	    
	    private GUIView widok;
	    private UIModel model;

	    public GUIController(GUIView view, UIModel model) {
	        this.widok = view;
	        this.model = model;
	        view.addMenuExitListener(new MenuExitListener());
	        view.addLoadButtonListener(new LoadButtonListener());
	        view.addBrowseButtonListener(new BrowseButtonListener());
	        view.addUpdateOnceButtonListener(new UpdateOnceButtonListener());
	        view.addUpdateToggleButtonListener(new UpdateToggleButtonListener());
	        view.addSpeedSliderListener(new SpeedSliderListener());
	    }

	    protected class MenuExitListener implements ActionListener {
	        public void actionPerformed(ActionEvent e) {
	            System.exit(0);
	        }
	    }

	    protected class LoadButtonListener implements ActionListener {
	        public void actionPerformed(ActionEvent e) {
	            try {
	                model.loadFile(widok.getFilePathText()); 
	                model.stopContinuosUpdate();
	                widok.visualBoard();
	            } catch (Exception ex) {
	            	widok.showError("Unable to open file.");
	                System.out.println("" + ex);
	            }
	        }
	    }

	    protected class BrowseButtonListener implements ActionListener {
	        public void actionPerformed(ActionEvent e) {
	            JFileChooser fc = new JFileChooser();
	            int returnval = fc.showOpenDialog(null);

	            if (returnval == JFileChooser.APPROVE_OPTION) {
	                File file = fc.getSelectedFile();
	                widok.setFilePath(file.getAbsolutePath());
	            }
	        }
	    }

	    protected class UpdateOnceButtonListener implements ActionListener {
	        public void actionPerformed(ActionEvent e) {
	            if (model.boardIsPopulated()) {
	                model.updateOnce();
	            }
	        }
	    }

	    protected class UpdateToggleButtonListener implements ActionListener {
	        public void actionPerformed(ActionEvent e) {
	            if (model.boardIsPopulated()) {
	                model.toggleContinuosUpdate();
	            }
	        }
	    }

	    protected class SpeedSliderListener implements ChangeListener {
	        public void stateChanged(ChangeEvent e) {
	            JSlider source = (JSlider)e.getSource();  
	            model.setSpeed((int)(source.getValue()));
	        }
	    }
	}
