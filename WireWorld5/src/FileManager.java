import java.util.*;
import java.io.*;

public class FileManager {

    private String filePath;
    private FileInputStream fileStream;
    private InputStreamReader in;
    private BufferedReader reader;

    private void openFile() throws FileNotFoundException {
        fileStream = new FileInputStream(filePath);
        in = new InputStreamReader(fileStream);
        reader = new BufferedReader(in);
    }


    private void closeFile() throws IOException {
        fileStream.close();
        in.close();
        reader.close();
    }


    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }


    public String getFilePath() {
        return this.filePath;
    }


     private Vector<Integer> readLine() throws IOException {
        Vector<Integer> chars = new Vector<Integer>();
        String line;
        line = reader.readLine();
        if (line == null) {
            return null;
        }
        for (int i = 0; i < line.length(); i++) {
            if (line.charAt(i) == '1') {
                chars.add(Integer.valueOf(1));
            } else if (line.charAt(i) == '2') {
                chars.add(Integer.valueOf(2));
            } else if (line.charAt(i) == '3') {
                chars.add(Integer.valueOf(3));
            } else {
                chars.add(Integer.valueOf(0));
            }
        }
        return chars;
     }

    public int[][] getBoard() throws IOException, FileNotFoundException {
        Vector<Vector<Integer>> chars = new Vector<Vector<Integer>>();
        int[][] board;
        int nOfRows;
        int nOfCols;
        Vector<Integer> line;

        openFile();
        while ((line = readLine()) != null) {
            chars.add(line);
        }
        closeFile();

        if (chars.size() == 0) {
            board = new int[1][1];
            board[0][0] = 0;
            return board;
        }
        nOfRows = chars.size();
        nOfCols = chars.get(0).size();
        if (nOfCols > 70) {
            nOfCols = 70;
        }
        board = new int[nOfRows][nOfCols];
        for (int i = 0; i < nOfRows; i++ ) {
            for (int j = 0; j < nOfCols; j++) {
                try {
                    board[i][j] = chars.get(i).get(j).intValue();
                } catch (ArrayIndexOutOfBoundsException e) {
                    board[i][j] = 0;
                }
            }
        }
        return board;
    }

}
