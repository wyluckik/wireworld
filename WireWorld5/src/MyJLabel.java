	import javax.swing.*;
	import java.awt.*;
	import java.util.Observer;
	import java.util.Observable;

	public class MyJLabel extends JLabel implements Observer{

	    public MyJLabel() {
	        super(" ");
	        this.setOpaque(true);
	        this.setPreferredSize(new Dimension(10,10));
	    }

	    public void update(Observable o, Object arg) {
	    	Cell a = (Cell)o;
	        if (a.getStan() == 0) {
	            this.setBackground(Color.black);
	        } else if (a.getStan() == 1) {
	            this.setBackground(Color.red);
	        } else if (a.getStan() == 2) {
	            this.setBackground(Color.blue);
	        } else {
	            this.setBackground(Color.orange);
	        }
	    }
	}

