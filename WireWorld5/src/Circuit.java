	public class Circuit {
	    
	    private Cell[][] tablica;
	    private int wiersz;
	    private int kolumna;
	    private FileManager file;
	    private Boolean isPopulated;

	    public Circuit(FileManager file) {
	        this.file = file;
	        this.isPopulated = false;
	    }

	    public Cell getCellIn(int row, int col) {
	        if ((row >= 0 && row <= this.wiersz)
	             && (col >= 0 && col <= this.kolumna)) {
	            return tablica[row+1][col+1];
	        } else {
	            return null;
	        }
	    }

	    public int getRows() {
	        return wiersz;
	    }

	    public int getColumns() {
	        return kolumna;
	    }

	    public Boolean isPopulated() {
	        return isPopulated;
	    }

	    public void populateFromFile(String filePath) throws Exception {
	        file.setFilePath(filePath);
	        int[][] intboard = file.getBoard();
	        this.wiersz = intboard.length;
	        this.kolumna = intboard[0].length;
	        this.tablica = new Cell[wiersz+2][kolumna+2];

	        for (int row = 0; row < wiersz; row++) {
	            for (int col = 0; col < kolumna; col++) {
	                this.tablica[row+1][col+1] = new Cell(intboard[row][col]);
	            }
	        }
	        setHoods();
	        this.isPopulated = true;
	   }

	    private void setHoods() {
	        for (int row = 1; row <= wiersz; row++) {
	            for (int col =1; col <= kolumna; col++) {
	            	Cell[] hood = {tablica[row-1][col-1], tablica[row-1][col],
	            			tablica[row-1][col+1], tablica[row][col-1],
	            			tablica[row][col+1], tablica[row+1][col-1],
	            			tablica[row+1][col], tablica[row+1][col+1]};
	            	tablica[row][col].setHood(hood);
	            	tablica[row][col].checkHood();
	            }
	        }
	   }
	}